/** Sorting of balls.
 * @since 1.8
 */
public class ColorSort {

   enum Color {red, green, blue};

   public static void main(String[] param) {
      Color[] balls;

      balls = new Color[15];

      for (int i = 0; i < balls.length; i++) {
         double rnd = Math.random();
         if (rnd < 1. / 3.) {
            balls[i] = Color.red;
            //rCount++;
         } else if (rnd > 2. / 3.) {
            balls[i] = Color.blue;
            //bCount++;
         } else {
            balls[i] = Color.green;
            //gCount++;
         }
      }
      for (int i = 0; i < balls.length; i++) {
         System.out.println(balls[i] + " : " + i);
      }
      System.out.println("over");
      reorder(balls);
      for (int i = 0; i < balls.length; i++) {
         System.out.println(balls[i]);
      }
      // for debugging
   }

   public static void reorder(Color[] balls) {
         int right = balls.length - 1;
         int left = 0;
         Color temp;
         Color red = Color.red;
         Color blue = Color.blue;
         for (int i = 0; i <= right; i++) {
            if (balls[i] == red) {
               temp = balls[i];
               balls[i] = balls[left];
               balls[left] = temp; // first color saved as red
               left++; // moves the next index, so red is not overwritten
            } else if (balls[i] == blue) {
               temp = balls[i];
               balls[i] = balls[right];
               i--;//moves the index back as i++ will cause to miss an object
               //saved in current [i] index when if statements are checked
               balls[right] = temp;// last index is saved as blue
               right--;//moves to the previous index to avoid overwriting saved blue
            }// TODO!!! Your program here
         }
      }


}
// https://self-learning-java-tutorial.blogspot.com/2014/08/sort-array-of-three-color-balls.html
